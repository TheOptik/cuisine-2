package fr.ib.cuisine;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

@SpringBootApplication
public class Application {

	private static final Logger logger = LoggerFactory.getLogger(Application.class);
	
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
		logger.info("Application correctement démarrée !");
		// les exceptions sont aussi dans le log
		// throw new RuntimeException("catastrophe ! ");
	}
	
	// Spring Boot web utilise "viewResolver" automatiquement
	@Bean(name="viewResolver")
	public ViewResolver getViewResolver() {
		InternalResourceViewResolver vr = new InternalResourceViewResolver();
		vr.setViewClass(JstlView.class); // type de vue : JSTL + JSP
		vr.setPrefix("/WEB-INF/vues/");
		vr.setSuffix(".jsp");
		return vr;
	}

}
