package fr.ib.cuisine;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller @Lazy
public class CuisineController {
private CuisineService cuisineService;
	
	@RequestMapping(path="/plats", method=RequestMethod.GET)
	public String afficherFormulaire(Model model) {
		model.addAttribute("service", cuisineService);
		return "plats";
	}
	
	@Autowired
	public void setCuisineService(CuisineService cuisineService) {
		this.cuisineService = cuisineService;
	}
}
